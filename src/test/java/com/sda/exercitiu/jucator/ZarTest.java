package com.sda.exercitiu.jucator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ZarTest {

    @Mock
    private Random random;

    @InjectMocks
    private Zar zar;

    @Test
    public void test_aruncaZar(){
        //given
        //Random r = Mockito.mock(Random.class);
        //Zar z = new Zar(r);
        when(random.nextInt(6)).thenReturn(5);
        int expected = 6;

        //when
        int actual = zar.aruncaZar();

        //then
        assertEquals(expected,actual);
    }

}
