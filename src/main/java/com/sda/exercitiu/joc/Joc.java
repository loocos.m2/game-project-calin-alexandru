package com.sda.exercitiu.joc;

import com.sda.exercitiu.exceptions.NoMoneyException;
import com.sda.exercitiu.jucator.Jucator;
import com.sda.exercitiu.jucator.Zar;

import java.util.Scanner;

public class Joc {
    private int runda = 0;
    private int miza;
    private Jucator jucator1;
    private Jucator jucator2;

    public Joc(Jucator jucator1, Jucator jucator2) {
        this.jucator1 = jucator1;
        this.jucator2 = jucator2;
    }

    public void rundaUrmatoare() {
        this.runda++;
    }

    public void seteazaMiza(Scanner scnMiza) {
        int miza = scnMiza.nextInt();
        try {
            this.valideazaMiza(miza);
            this.miza = miza;
        } catch (NoMoneyException e) {
            this.miza = 0;
        }
    }

    public void valideazaMiza(int miza) {
        if ((miza > this.jucator1.getSuma()) || (miza > this.jucator2.getSuma())) {
            throw new NoMoneyException("Nu ai banii!! :)");
        }
    }

    public void updateazaJucatorii(int valoareZar1, int valoareZar2) {
        if (valoareZar1 == valoareZar2) {

        } else if (valoareZar1 > valoareZar2) {
            this.jucator1.updateazaSuma(this.miza);
            this.jucator2.updateazaSuma(-this.miza);
            System.out.println("Castigatorul este " + this.jucator1.getNume());
        } else {
            this.jucator2.updateazaSuma(this.miza);
            this.jucator1.updateazaSuma(-this.miza);
            System.out.println("Castigatorul este " + this.jucator2.getNume());
        }
    }

    public void play() {
        this.rundaUrmatoare();
        System.out.println("***\t Runda " + this.runda + "\t ***");
        Scanner scanner = new Scanner(System.in);
        this.seteazaMiza(scanner);
        System.out.println("Miza " + this.miza + " RON");
        int zarJucator1 = this.jucator1.aruncaZaruri(new Zar());
        int zarJucator2 = this.jucator2.aruncaZaruri(new Zar());
        this.updateazaJucatorii(zarJucator1, zarJucator2);
        System.out.println(this.jucator1.getNume() + " are " + this.jucator1.getSuma() + " RON");
        System.out.println(this.jucator2.getNume() + " are " + this.jucator2.getSuma() + " RON");
        System.out.println("******************************");
    }
}
